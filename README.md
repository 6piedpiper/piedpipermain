# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is for documents and code related to the project

### How do I get set up? ###

1. Create a project in eclipse.
2. In bitbucket click on clone and copy the address, but not the commands.
3. Open git bash.
4. Use cd to navigate to where the project folder is.
5. Use git init to create your local repo
6. Type git remote add origin [the address you copied]
7. Type git pull origin master
8. Type git status and you should get, working directory clean in the resulting text, if not then go to troubleshooting below.
9. Type git branch [nameyourbranchhere].
10. Type git checkout [nameyourbranchhere].


### Troubleshooting ###

*If at step 7 you saw 
.classpath
.project
then check the physical folder for the file .gitignore, if it is not there then take the file from bitbucket and paste it into the project folder. Type git status again and it should return ok.
### Contribution guidelines ###

Here are the steps for adding new changes to the repo (eclipse on the left and gitbash on the right) both are good :)

1. Commit the work in your local branch. 	--> git add . & git commit -m (your message)
2. Switch to the master branch. 			--> git checkout master
3. Pull down the remote master branch.		--> git pull origin master
4. Merge the local branch into the master branch. --> git merge localbranch
5. Resolve any merge issues if they arise, (this is rare). 
6. Push the master branch to the remote master. --> git push origin master
7. Switch to your local branch. --> git checkout localbranch
7. Merge your master branch into the local.	--> git merge master
8. If an error says that the remote master has been changed simply repeat steps 
3 - 6 and this should resolve it.

If you have any questions with the above please ask James.

### Who do I talk to? ###

* James for any problems involving git or the repo.

### Definition of done ###
1. Unit test return success for all user story functions where unit tests are appropriate. 
2. Manual tests return success for all user story functions where unit tests are appropriate and are documented. 
3. Functions are given the correct precedence in the shunting yard. 
4. User stories are successully push to the repo. 
5. The commit for the completed user story has the complete user story and name of person. 
6. The User story is marked done in the excel sheet. 
7. Any outstanding bugs are put into the bugs excel.
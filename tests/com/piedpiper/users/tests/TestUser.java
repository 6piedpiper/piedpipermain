package com.piedpiper.users.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.piedpiper.users.User;
import com.piedpiper.users.UserRole;

public class TestUser {

	private int empId;
	private String userName,  pWord;
	private User user1;
	
	@Before
	public void setup(){
		user1= new User();
		userName="user"; 
		pWord="password";
		empId=10;
	}
	
	@Test
	public void testGetsAndSets() {
		user1.setEmpId(empId);
		user1.setpWord(pWord);
		user1.setUserName(userName);
		user1.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		
		assertEquals(user1.getEmpId(),10,.1);
		assertEquals(user1.getUserName(), "user");
		assertEquals(user1.getpWord(), "password");
		assertEquals(user1.getUserRole(), "CUSTOMER_SERVICE_REPRESENTATIVE");
	}
	
	@Test
	public void testEnum(){
		user1.setUserRole(UserRole.NETWORK_MANAGEMENT_ENGINEER);
		assertEquals(user1.getUserRole(),"NETWORK_MANAGEMENT_ENGINEER");
		user1.setUserRole(UserRole.SUPPORT_ENGINEER);
		assertEquals(user1.getUserRole(),"SUPPORT_ENGINEER");
		user1.setUserRole(UserRole.SYSTEM_ADMINISTRATOR);
		assertEquals(user1.getUserRole(),"SYSTEM_ADMINISTRATOR");
	}
	
	@Test
	public void testToString(){
		user1.setEmpId(empId);
		user1.setpWord(pWord);
		user1.setUserName(userName);
		user1.setUserRole(UserRole.CUSTOMER_SERVICE_REPRESENTATIVE);
		assertEquals(user1.toString(),"User [id= " + 10 + ", User Name= user, hashCode= " + 10 + "]");
	}
	
	
	@Test
	public void testEquals(){
		User user2=new User();
		user1.setEmpId(empId);
		user2.setEmpId(10);
		assertTrue(user1.equals(user2));
		
		user2.setEmpId(20);
		assertFalse(user1.equals(user2));
		
		
		assertFalse(user1.equals(new Object()));
	}

}

package com.piedpiper.dataimport.tabObjects;

import static com.piedpiper.dataimport.ExcelReader.getIntValues;
import static com.piedpiper.dataimport.ExcelReader.getStringValues;

import java.io.Serializable;
import java.text.ParseException;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.piedpiper.dataimport.ExcelTabDataObject;
/**
 * @author James Runswick
 * @author Date Created 23 May 2016
 * @version 1.0
 *
 */
@IdClass(EventCausePK.class)
@Entity(name ="event_cause")
public class EventCauseData implements ExcelTabDataObject {
	@Id //@GeneratedV4dalue(strategy=GenerationType.IDENTITY)
	private int causeCode;
	@Id
	private int eventId;
	private String description;

	public EventCauseData() {

	}

	@Override
	public void RetrieveObjectFromExcel(Row row) throws ParseException {
		for (Cell cell: row){
			switch (cell.getColumnIndex()) {
			case 0:
//				System.out.println(cell.getDateCellValue());
				this.causeCode = getIntValues(cell);
//				System.out.println(date);
				break;
			case 1:
				this.eventId = getIntValues(cell);
				break;
			case 2:
				this.description = getStringValues(cell);
				break;
			}
		}
	}

	

	/**
	 * @return the causeCode
	 */
	public int getCauseCode() {
		return causeCode;
	}

	/**
	 * @param causeCode the causeCode to set
	 */
	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}

	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}

class EventCausePK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int causeCode;
	int eventId;
	/**
	 * @return the causeCode
	 */
	public int getCauseCode() {
		return causeCode;
	}
	/**
	 * @param causeCode the causeCode to set
	 */
	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}
	/**
	 * @return the eventId
	 */
	public int getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
}

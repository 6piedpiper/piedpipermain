package com.piedpiper.dataimport;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.piedpiper.dataimport.tabObjects.BaseData;
import com.piedpiper.dataimport.tabObjects.EventCauseData;
import com.piedpiper.dataimport.tabObjects.MachineCodeData;
import com.piedpiper.dataimport.tabObjects.UserEntity;

/**
 * @author James Runswick
 * @author Date Created 26 May 2016
 * @version 1.0
 *
 */
public class ExcelReader {
	public static void readExcelFileToDB(InputStream fileContent, ImportExcelDAO databaseAcesss) {
		try (Workbook wb = new HSSFWorkbook(fileContent);) {
			ExcelTabDataObject newDataObject = null;
			for (Sheet sheet : wb) {
				newDataObject = createEntityForSheet(newDataObject, sheet);
				if (newDataObject == null) // skips non-matching sheets
					continue;
				for (Row row : sheet) {
					if (row.getRowNum() == 0) //skip the excel data headers in the first row
						continue;
					newDataObject.RetrieveObjectFromExcel(row);
					if(newDataObject instanceof BaseData) {
						BaseData baseDataObject = (BaseData) newDataObject;
						DataValidator validator = DataValidator.getValidatorInstance(databaseAcesss);  
						if(validator.validateBaseDataObject(baseDataObject)){
							databaseAcesss.addSingleEntity(baseDataObject);
						}
					}else{
					databaseAcesss.addSingleEntity(newDataObject);
					}
				}
			}
			System.out.print("row complete");
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static ExcelTabDataObject createEntityForSheet(ExcelTabDataObject newDataObject, Sheet sheet) {
		if (sheet.getSheetName().equals("MCC - MNC Table"))
			return new MachineCodeData();
		if (sheet.getSheetName().equals("UE Table"))
			return new UserEntity();
		if (sheet.getSheetName().equals("Event-Cause Table"))
			return new EventCauseData();
		if (sheet.getSheetName().equals("Base Data"))
//			return new BaseData();
			return null;
		return null;
	}
	
	public static int getIntValues(Cell cell){
		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC) 
			return (int) cell.getNumericCellValue();
		return 0;
	}
	
	public static String getStringValues(Cell cell){
		if(cell.getCellType() == Cell.CELL_TYPE_STRING) 
			return cell.getStringCellValue();
		return null;
	}
	
	public static long getLongValue(Cell cell){
		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC) 
			return (long) cell.getNumericCellValue();
		return 0;
	}
}
